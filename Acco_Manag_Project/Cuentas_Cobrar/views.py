from django.shortcuts import render, redirect
from .models import Cuenta, Departamento, Compania, TipoCuenta, Periodo
from django.contrib import messages
from General.views import accion_bitacora

def cuentas_cobrar(request):
    countsList = Cuenta.objects.all()
    return render(request, "cuentas_por_cobrar.html", {'counts': countsList})


def form_nueva_factura(request):
    periods = Periodo.objects.all()    
    companies = Compania.objects.all()
    countType = TipoCuenta.objects.all()
    return render(request, "form_nueva_factura.html", {'periods': periods, 'companies': companies, 'countType': countType})


def register_account(request, *args, **kwargs):
    date = Periodo.objects.get(id=int(request.POST.get('IdPeriodo', False)))
    company = Compania.objects.get(id=int(request.POST.get('IdCompania', False)))    
    countType = TipoCuenta.objects.get(id=int(request.POST.get('IdTipoCuenta', False)))
    invoice = request.POST.get('CodigoFactura', False).replace(",.","")
    amount = request.POST.get('Monto', False).replace(",.","")

    Cuenta.objects.create(
            IdPeriodo=date,
            IdCompania=company,             
            IdTipoCuenta=countType, 
            CodigoFactura=invoice, 
            Monto=amount
            )
    accion_bitacora(request,'Crear Cuenta','Se ha registrado una cuenta '+str(countType))    
    messages.success(request, 'Cuenta Agregada!')
    return redirect('/cuentas_cobrar')


def edit_active_account(request, id):
    account = Cuenta.objects.get(id=id)
    periods = Periodo.objects.all()    
    companies = Compania.objects.all()
    countType = TipoCuenta.objects.all()
    return render(request,'editar_cuenta_cobrar.html', {'account': account, 'periods': periods, "companies":companies, 'countType': countType, 'id':id})


def update_account(request, id, *args, **kwargs):    
    date = Periodo.objects.get(id=int(request.POST.get('IdPeriodo', False)))
    company = Compania.objects.get(id=int(request.POST.get('IdCompania', False)))    
    countType = TipoCuenta.objects.get(id=int(request.POST.get('IdTipoCuenta', False)))
    invoice = request.POST.get('CodigoFactura', False)
    invoice = invoice.replace(",.","")
    amount = request.POST.get('Monto', False)
    amount = amount.replace(",.","")

    Account=Cuenta.objects.get(id=id)        
    Account.IdCompania=company
    Account.IdTipoCuenta=countType
    Account.CodigoFactura=invoice
    Account.Monto=amount
    Account.IdPeriodo=date
    Account.save()

    accion_bitacora(request,'Modificar Cuenta','Se ha modificado una cuenta '+str(countType))    
    messages.success(request, '¡Cuenta Actualizada!')
    return redirect('/cuentas_cobrar')


def edit_account(request):
    return render(request, "editar_cuenta_cobrar.html")


def delete_account(request, id):
    count = Cuenta.objects.get(id=id)
    count.delete()        
    accion_bitacora(request,'Eliminar Cuenta','Se ha eliminado una cuenta '+str(count.IdTipoCuenta))    
    messages.success(request, 'Cuenta Eliminada!')
    return redirect('/cuentas_cobrar')