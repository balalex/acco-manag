from django.urls import path
from . import views

urlpatterns = [
    path('', views.periodos, name="periodos"),
    path('registro',views.registrar_periodo),
    path('editar_estado/<id>',views.editar_estado, name="Cerrar_Periodo")
]