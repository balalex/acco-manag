from django.shortcuts import render

# Create your views here.
def balance_general(request):
    return render(request,"balance_general.html")

def form_balance_general(request):
    return render(request,"form_balance_general.html")