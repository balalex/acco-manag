from django.contrib import admin
from .models import  Puesto,TipoBeneficio,TipoCedula, Empleado, Compania ,Departamento,Nomina
from Periodos.models import Periodo
from Login.models import Rol
# Register your models here.


admin.site.register(Puesto)
admin.site.register(TipoBeneficio)
admin.site.register(TipoCedula)
admin.site.register(Empleado)
admin.site.register(Compania)
admin.site.register(Departamento)
admin.site.register(Nomina)
