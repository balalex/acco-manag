from django.urls import path
from . import views

urlpatterns = [
    path('', views.activos_fijos, name="activos_fijos"),
    path('eliminar_activo_fijo/<id>',views.eliminar_activo_fijo, name="eliminar_activo_fijo"),
    path('edicion_activo_fijo/<id>',views.edicion_activo_fijo, name="edicion_activo_fijo"),
    path('edicion_activo_fijo/editar_activo_fijo/<id>',views.editar_activo_fijo)

]


