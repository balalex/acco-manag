from django.shortcuts import render
from multiprocessing import context
from django.contrib import messages
from multiprocessing.spawn import import_main_path
from pickle import NONE
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .models import ActivoFijo, Departamento, TipoActivo,CuentaBancaria
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password
from General.Sender import Sender_Email
from General.views import accion_bitacora
# Create your views here.
def activos_fijos(request):
    activosLista = ActivoFijo.objects.all()
    
    return render(request,"activos_fijos.html",{'ActivoFijo':activosLista})


def form_activo_fijo(request):
    Departamentos= Departamento.objects.all()
    TipoActivos = TipoActivo.objects.all()
    CuentasBancarias= CuentaBancaria.objects.all()
    return render(request,"form_activo_fijo.html",{'Departamentos':Departamentos,'TipoActivo':TipoActivos,'CuentaBancaria': CuentasBancarias})

def registrar_activo_fijo(request, *args, **kwargs):
    departamento = Departamento.objects.get(id=int(request.POST.get('departamento',False)))
    codigo = request.POST.get('codigo',False)
    descripcion = request.POST.get('descripcion',False)
    tipo_activo = TipoActivo.objects.get(id=int(request.POST.get('tipo_activo',False)))
    CentroCosto = request.POST.get('CentroCosto',False)
    cuenta_bancaria = CuentaBancaria.objects.get(id=int(request.POST.get('cuenta_bancaria',False)))
    Debito = request.POST.get('Debito',False)
    Credito = request.POST.get('Credito',False)
    ActivoFijo.objects.create(IdDepartamento=departamento, CodigoActivo=codigo,DescripcionActivo=descripcion,IdTipoActivo=tipo_activo, CentroCosto=CentroCosto,CuentaBancaria=cuenta_bancaria,Debito=Debito,Credito=Credito)    
    accion_bitacora(request,'Crear Activo Fijo','Se ha registrado el activo fijo '+codigo)
    messages.success(request, '¡Activo Fijo Agregado!')
    return redirect('/activos_fijos')

def eliminar_activo_fijo(request, id):
    Activo = ActivoFijo.objects.get(id=id)
    Activo.delete()            
    accion_bitacora(request,'Eliminar Activo Fijo','Se ha eliminado el activo fijo '+Activo.CodigoActivo)
    messages.success(request, '¡Activo Fijo Eliminado!')
    return redirect('/activos_fijos')

def edicion_activo_fijo(request, id):
    Activo = ActivoFijo.objects.get(id=id)
    departamentosListados = Departamento.objects.all()
    tipo_activosListados = TipoActivo.objects.all()
    cuentas_bancariasListados = CuentaBancaria.objects.all()
    return render(request,'editar_activo_fijo.html', {'ActivoFijo':Activo,"Departamentos":departamentosListados,"TipoActivo":tipo_activosListados,"CuentaBancaria":cuentas_bancariasListados,'id':id})

def editar_activo_fijo(request, id, *args, **kwargs):
    departamento = Departamento.objects.get(id=int(request.POST.get('departamento',False)))
    codigo = request.POST.get('codigo',False)
    descripcion = request.POST.get('descripcion',False)
    tipo_activo = TipoActivo.objects.get(id=int(request.POST.get('tipo_activo',False)))
    CentroCostoText =request.POST.get('CentroCosto',False)
    CentroCosto= float(CentroCostoText.replace(",","."))
    cuenta_bancaria = CuentaBancaria.objects.get(id=int(request.POST.get('cuenta_bancaria',False)))
    DebitoText = request.POST.get('Debito',False)
    Debito= float(DebitoText.replace(",","."))
    CreditoText = request.POST.get('Credito',False)
    Credito = float(CreditoText.replace(",","."))
 
    Activo=ActivoFijo.objects.get(id=id)    
    Activo.IdDepartamento=departamento
    Activo.CodigoActivo=codigo
    Activo.DescripcionActivo=descripcion
    Activo.IdTipoActivo=tipo_activo
    Activo.CentroCosto=CentroCosto
    Activo.CuentaBancaria=cuenta_bancaria
    Activo.Debito=Debito
    Activo.Credito=Credito
    Activo.save()
    
    accion_bitacora(request,'Modificar Activo Fijo','Se ha modificado el activo fijo '+Activo.CodigoActivo)
    messages.success(request, '¡Activo Fijo Actualizado!')
    return redirect('/activos_fijos')
