from multiprocessing import context
from django.contrib import messages
from multiprocessing.spawn import import_main_path
from pickle import NONE
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .models import Empleado, Nomina, Departamento, Compania, Periodo
from .forms import EmpleadoForm
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password
from General.Sender import Sender_Email
from General.views import accion_bitacora

# Create your views here.
def nominas(request):
    nominasList = Nomina.objects.all()
    return render(request,"nominas.html",{'Nominas':nominasList})

def form_nomina(request):
    periodo = Periodo.objects.all()
    compania = Compania.objects.all()
    empleado = Empleado.objects.all()
    departamento = Departamento.objects.all()
    return render(request,"form_nomina.html",{'Periodos':periodo,'Compañias':compania,'Empleados':empleado,'Departamentos':departamento})

def editar_nomina(request):
    return render(request,"editar_nomina.html")

def registrar_nomina(request, *args, **kwargs):
    periodo = Periodo.objects.get(id=int(request.POST.get('periodo',False)))    
    compania = Compania.objects.get(id=int(request.POST.get('compañia',False)))
    empleado = Empleado.objects.get(Cedula=request.POST.get('empleado',False))    
    departamento = Departamento.objects.get(id=int(request.POST.get('departamento',False)))
    monto = request.POST.get('monto',False)    
    
    Nomina.objects.create(IdPeriodo=periodo, IdCompania=compania,IdEmpleado=empleado,IdDepartamento=departamento,Monto=monto)
    accion_bitacora(request,'Crear Nómina','Se ha registrado una nómina para el empleado '+empleado.Cedula)    
    messages.success(request, '¡Nómina Creada!')
    return redirect('/nominas')

def eliminar_nomina(request, id):
    nomina = Nomina.objects.get(id=id)
    nomina.delete()        
    accion_bitacora(request,'Eliminar Nómina','Se ha eliminado una nómina para el empleado '+str(nomina.IdEmpleado))    
    messages.success(request, 'Nómina Eliminada!')
    return redirect('/nominas')

def edicion_nomina(request, id):
    nomina=Nomina.objects.get(id=id)    
    periodosListados = Periodo.objects.all()
    companiasListados = Compania.objects.all()
    departamentosListados = Departamento.objects.all()
    return render(request,'editar_nomina.html', {'Nomina':nomina,"Periodos":periodosListados,"Compañias":companiasListados, "Departamentos":departamentosListados,'id':id})

def editar_nomina(request, id, *args, **kwargs):
    periodo = Periodo.objects.get(id=int(request.POST.get('periodo',False)))    
    compania = Compania.objects.get(id=int(request.POST.get('compañia',False)))
    empleado = Empleado.objects.get(Cedula=request.POST.get('empleado',False))    
    departamento = Departamento.objects.get(id=int(request.POST.get('departamento',False)))
    monto = request.POST.get('monto',False) 
    monto = monto.replace(",","")   
    
    nomina=Nomina.objects.get(id=id)    
    nomina.IdPeriodo=periodo
    nomina.IdCompania=compania
    nomina.IdEmpleado=empleado
    nomina.IdDepartamento=departamento
    nomina.Monto=monto
    nomina.save()

    accion_bitacora(request,'Modificar Nómina','Se ha modificado una nómina del empleado '+empleado.Cedula)    
    messages.success(request, '¡Nómina Actualizada!')
    return redirect('/nominas')


class EmpleadoCreate(CreateView):
    model = Empleado
    template_name = 'empleados.html'
    form_class = EmpleadoForm    
    success_url = nominas

    def get_context_data(self, **kwargs):
        context = super(EmpleadoCreate, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        if form.is_valid():
            empleado = form.save(commit=False)
            nombre = request.POST.get('Nombre',NONE)
            apellido = request.POST.get('PrimerApellido',NONE)
            cedula = request.POST.get('Cedula',NONE)
            correo = request.POST.get('Correo',NONE)
            usuario = nombre[0:3]+apellido[0:3].lower()+cedula[-4:]
            password = get_random_string(length=12)            
            password_hash = make_password(password)
            nombreCompleto = nombre+' '+apellido
            empleado.IdUsuario = User.objects.create(username = usuario , password = password_hash,email=correo)
            empleado.save()
            accion_bitacora(request,'Crear Empleado','Se ha registrado al empleado '+cedula)   
            accion_bitacora(request,'Crear Usuario','Se ha registrado el usuario '+usuario)             
            print(password)
            Sender_Email(correo, nombreCompleto,usuario, password)
            empleadosList=Empleado.objects.all()
            return redirect("/home")
        else:
            return self.render_to_response(self.get_context_data(form=form))
