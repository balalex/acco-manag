from django.urls import path
from . import views

urlpatterns = [
    path('', views.cuentas_pagar, name="cuentas_pagar")
]