from django.apps import AppConfig


class BalanceGeneralConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Balance_General'
