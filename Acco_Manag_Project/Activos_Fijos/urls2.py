from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_activo_fijo, name="form_activo_fijo"),
    path('crear_activo_fijo', views.registrar_activo_fijo, name="crear_activo_fijo")
]


