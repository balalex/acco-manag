from django.contrib import admin
from .models import  Banco
from Nominas.models import TipoCliente, Cliente


# Register your models here.
admin.site.register(TipoCliente)
admin.site.register(Cliente)
admin.site.register(Banco)