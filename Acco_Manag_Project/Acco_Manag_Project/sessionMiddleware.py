from django.contrib.auth.models import User, auth
from django.shortcuts import render
from difflib import SequenceMatcher

def custom404(request, exception, template_name='errorPage.html'):
        response = render(request, template_name)
        response.status_code = 404
        return response

class SessionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        baseURL = request.build_absolute_uri()
        #response = self.get_response(request)

        try:
            sessionToken = request.session['token']
        except:
            sessionToken = ""
        
        response = self.get_response(request)
        return response
        
            
