
from django.contrib import admin

from .models import  TipoCuenta,Cuenta
from Nominas.models import Compania, Departamento
from Periodos.models import Periodo
# Register your models here.

admin.site.register(TipoCuenta)
admin.site.register(Cuenta)