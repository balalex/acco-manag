from django.apps import AppConfig


class CuentaBancariaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Cuenta_Bancaria'
