from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_banco, name="form_banco"),
    path('crear_banco', views.registrar_banco, name="crear_banco")
]