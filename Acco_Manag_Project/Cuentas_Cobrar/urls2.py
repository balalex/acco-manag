from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_nueva_factura, name="form_nueva_factura"),
    path('register_account', views.register_account, name="register_account")
]