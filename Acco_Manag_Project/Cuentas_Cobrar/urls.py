from django.urls import path
from . import views

urlpatterns = [
    path('', views.cuentas_cobrar, name="cuentas_cobrar"),
    path('eliminar_cuenta/<id>',views.delete_account, name="eliminar_cuenta"),
    path('edicion_cuenta/<id>',views.edit_active_account, name="edicion_cuenta"),
    path('edicion_cuenta/editar_cuenta/<id>',views.update_account)
]