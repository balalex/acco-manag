from django.db import models
from django.contrib.auth.models import User
# Create your models here.
  
class Bitacora(models.Model):
  IdUsuario= models.ForeignKey(User, on_delete=models.CASCADE)
  Accion = models.CharField(max_length = 50)
  Descripcion = models.TextField()
  Fecha = models.DateTimeField()
  
  def __str__(self):
        return self.Descripcion
  