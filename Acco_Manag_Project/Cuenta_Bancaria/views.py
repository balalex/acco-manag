from http import client
from django.contrib import messages
from django.shortcuts import redirect, render
from .models import CuentaBancaria, Banco, Compania
from General.views import accion_bitacora
import hashlib
# Create your views here.
def cuentas_bancarias(request):
    cuentasListados = CuentaBancaria.objects.all()
    bancoListados = Banco.objects.all()
    companiasListados = Compania.objects.all()
    return render(request,"cuentas_bancarias.html", {"Cuentas": cuentasListados, "Bancos": bancoListados,"companias": companiasListados})    

def editar_cuenta_bancaria(request):
    return render(request,"editar_cuenta_bancaria.html")

def registrar_cuenta(request, *args, **kwargs):
    nombre_completo = request.POST.get('nombre',False)    
    Codigo_cvv = hashlib.sha256( (request.POST.get('cvv',False)).encode('utf-8')).hexdigest()
    IdBanco= int(request.POST.get('banco', False))
    banco = Banco.objects.get(id=IdBanco)
    CuentaIBAN= request.POST.get('iban',False) 
    compania = Compania.objects.get(id=int(request.POST.get('compania',False)))
    NumCuenta= request.POST.get('numero_cuenta', False)    
    numContrato= request.POST.get('numero_contrato', False)    

    CuentaBancaria.objects.create(Nombre_Tarjeta=nombre_completo,Codigo_cvv=Codigo_cvv,IdBanco=banco,CuentaIBAN=CuentaIBAN,IdCompania=compania,NumCuenta=NumCuenta,numContrato=numContrato)
    messages.success(request, '¡Cuenta Creada!')
    return redirect('/cuentas_bancarias')

def edicion_cuenta(request, id):
    cuenta = CuentaBancaria.objects.get(id=id)
    bancoListados = Banco.objects.all()
    companiaListados = Compania.objects.all()
    return render(request,'editar_cuenta_bancaria.html', {'Cuenta':cuenta,"Bancos":bancoListados,"Compania": companiaListados, 'id':id})

def editar_cuenta(request, id, *args, **kwargs):
    nombre_completo = request.POST.get('nombre',False)    
    Codigo_cvv = hashlib.sha256( (request.POST.get('cvv',False)).encode('utf-8')).hexdigest()   
    IdBanco= int(request.POST.get('banco', False))
    banco = Banco.objects.get(id=IdBanco)
    CuentaIBAN= request.POST.get('iban',False) 
    compania = Compania.objects.get(id=int(request.POST.get('compania',False)))  
    NumCuenta= request.POST.get('numero_cuenta', False)    
    numContrato= request.POST.get('numero_contrato', False)    

    cuenta=CuentaBancaria.objects.get(id=id)    
    cuenta.Nombre_Tarjeta=nombre_completo
    cuenta.Codigo_cvv=Codigo_cvv
    cuenta.IdBanco=banco
    cuenta.CuentaIBAN=CuentaIBAN
    cuenta.IdCompania=compania
    cuenta.NumCuenta=NumCuenta
    cuenta.numContrato=numContrato
    cuenta.save()

    messages.success(request, '¡Cuenta Actualizada!')
    return redirect('/cuentas_bancarias')

def eliminar_cuenta(request, id):
    cuenta = CuentaBancaria.objects.get(id=id)
    cuenta.delete()        
    messages.success(request, '¡Cuenta Eliminada!')
    return redirect('/cuentas_bancarias')