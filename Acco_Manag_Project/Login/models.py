from django.db import models

# Create your models here.
class Permiso(models.Model):
  Permiso = models.CharField(max_length= 30)
  Descripcion = models.CharField(max_length =50)

  def __str__(self):
        return self.Permiso

class Rol(models.Model):
  Rol = models.CharField(max_length=30)
  Descripcion = models.TextField()
  Permiso = models.ManyToManyField(Permiso)

  def __str__(self):
        return self.Rol
