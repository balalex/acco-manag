from django.urls import path

from . import views

urlpatterns = [
    path('', views.login, name="login"),
    path("login", views.login, name="login"),
    path("logout", views.logout, name="logout"),
    path('recuperar_contrasena', views.recuperar_contrasena, name="recuperar_contrasena"),
    path('generateCode', views.sendCode, name="generateCode"),
    path('changePassword', views.changePassword, name="changePassword"),
    path('error_404', views.error_404, name="error_404"),
]
