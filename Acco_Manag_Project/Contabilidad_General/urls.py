from django.urls import path
from . import views

urlpatterns = [
    path('', views.contabilidad_general, name="contabilidad_general"),
    path('edicion_balance/<id>',views.edit_balance, name="edicion_balance"),
    path('edicion_balance/update_balance/<id>',views.update_balance),
    path('eliminar_balance/<id>',views.delete_balance, name="eliminar_balance")
]