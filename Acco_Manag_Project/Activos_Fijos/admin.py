from django.contrib import admin

from .models import  ActivoFijo, TipoActivo

from Nominas.models import Departamento
# Register your models here.

admin.site.register(ActivoFijo)
admin.site.register(TipoActivo)
