from django.shortcuts import render

# Create your views here.
def cuentas_pagar(request):
    return render(request,"cuentas_por_pagar.html")

def form_nueva_factura_pagar(request):
    return render(request,"form_nueva_factura_pagar.html")    