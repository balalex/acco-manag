import imp
from django.db import models
from Nominas.models import Departamento
from Cuenta_Bancaria.models import CuentaBancaria

# Create your models here.

class TipoActivo(models.Model):
  TipoActivo =models.CharField(max_length=50)
  Descripcion = models.TextField()

  def __str__(self):
        return self.TipoActivo

class ActivoFijo(models.Model):
  IdDepartamento = models.ForeignKey(Departamento,on_delete=models.CASCADE)
  CodigoActivo = models.CharField(max_length=20)
  DescripcionActivo= models.TextField()
  IdTipoActivo= models.ForeignKey(TipoActivo,on_delete=models.CASCADE)
  CentroCosto= models.FloatField()
  CuentaBancaria = models.ForeignKey(CuentaBancaria,on_delete=models.CASCADE, default=None, null=False, blank=False)
  Debito = models.FloatField()
  Credito = models.FloatField()

  def __str__(self):
        return self.DescripcionActivo

  