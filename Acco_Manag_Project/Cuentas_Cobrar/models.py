from datetime import datetime
from django.db import models
from Periodos.models import Periodo
from Nominas.models import Compania, Departamento
from django.utils import timezone
# Create your models here.


class TipoCuenta(models.Model):
  TipoCuenta = models.CharField(max_length=50)
  Descripcion= models.TextField()

  def __str__(self):
        return self.TipoCuenta

class Cuenta(models.Model): 
  IdPeriodo= models.ForeignKey(Periodo,on_delete=models.CASCADE, default=None, null=False, blank=False)
  IdCompania=models.ForeignKey(Compania,on_delete=models.CASCADE)
  IdTipoCuenta=models.ForeignKey(TipoCuenta,on_delete=models.CASCADE)
  CodigoFactura=models.IntegerField()
  Monto= models.FloatField()

  def __str__(self):
        return self.CodigoFactura
