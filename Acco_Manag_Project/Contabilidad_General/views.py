from django.shortcuts import render, redirect
from .models import  Periodo, CuentaBancaria, BalanceGeneral
from Nominas.models import Cliente
from django.contrib import messages
from General.views import accion_bitacora
def contabilidad_general(request):
    balances = BalanceGeneral.objects.all()
    return render(request, "contabilidad_general.html", {'balances': balances})


def form_agregar_balance(request):
    periods = Periodo.objects.all()
    accounts = CuentaBancaria.objects.all() 
    return render(request, "form_agregar_balance.html", {'periods': periods,  'accounts': accounts})


def register_balance(request, *args, **kwargs):
    date = Periodo.objects.get(id=int(request.POST.get('IdPeriodo', False)))
    costCenter = request.POST.get('costCenter', False)
    bankAcc = CuentaBancaria.objects.get(id=int(request.POST.get('IdCuentaBan', False)))
    credit = request.POST.get('credit', False)
    debit = request.POST.get('debit', False)
    invoice = request.POST.get('invoice', False)

    BalanceGeneral.objects.create(
        IdPeriodo=date,
        CentroCosto=costCenter,
        IdCuentaBan=bankAcc,
        CodFactura=invoice,
        Debito=debit,
        Credito=credit)
    accion_bitacora(request,'Crear Balance','Se ha registrado un balance ')
    messages.success(request, 'Balance Agregado!')
    return redirect('/contabilidad_general')


def edit_balance(request, id):
    balance = BalanceGeneral.objects.get(id=id)
    periods = Periodo.objects.all()
    accounts = CuentaBancaria.objects.all()  
    return render(request, "edit_balance.html", {'balance':balance, 'periods': periods, 'accounts': accounts})


def update_balance(request, id, *args, **kwargs):
    period = Periodo.objects.get(id=int(request.POST.get('IdPeriodo', False)))
    costCenter = request.POST.get('costCenter', False)
    bankAcc = CuentaBancaria.objects.get(id=int(request.POST.get('IdCuentaBan', False)))
    credit = request.POST.get('credit', False)
    debit = request.POST.get('debit', False)
    invoice = request.POST.get('invoice', False)

    balance = BalanceGeneral.objects.get(id=id)
    balance.IdPeriodo = period
    balance.CentroCosto = costCenter
    balance.IdCuentaBan = bankAcc
    balance.CodFactura = invoice
    balance.Debito = debit
    balance.Credito = credit
    balance.save()
    
    accion_bitacora(request,'Modificar Balance','Se ha modificado un balance ')
    messages.success(request, '¡Balance Actualizado!')
    return redirect('/contabilidad_general')


def edit_balance_page(request):
    return render(request, "editar_balance.html")


def delete_balance(request, id):
    balance = BalanceGeneral.objects.get(id=id)
    balance.delete()    
    accion_bitacora(request,'Eliminar Balance','Se ha eliminado un balance')
    messages.success(request, 'Balance Eliminado!')
    return redirect('/contabilidad_general')
