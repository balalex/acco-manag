
from django.db import models
from Cuenta_Bancaria.models import CuentaBancaria
from Periodos.models import Periodo
from Nominas.models import Compania, Departamento

class BalanceGeneral(models.Model):
  IdPeriodo= models.ForeignKey(Periodo,on_delete=models.CASCADE)  
  CentroCosto= models.FloatField()
  IdCuentaBan= models.ForeignKey(CuentaBancaria,on_delete=models.CASCADE)  
  CodFactura = models.CharField(max_length=10)
  Debito=models.FloatField()
  Credito= models.FloatField()

  def __str__(self):
        return self.IdCliente
