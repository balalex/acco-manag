from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_nueva_factura_pagar, name="form_nueva_factura_pagar")
]