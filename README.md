Procesos de instalación Python, Virtualenv y Django:

- Instalar Python:
	-Descargar python 3.10.4 --> https://www.python.org/ftp/python/3.10.4/python-3.10.4-amd64.exe
	-Seleccionar la opción de add path al momento de iniciar la instalación y en modo personalizar la instalación agregarlo en una carpeta de
		 Python310 en la raíz.
	-Luego en variables de entorno del windows o MACos si lo tiene revisar si se agregó un path de python310 y sino crear una variable que sea tipo C:\Python310


- Instalar Virtualenv en la raíz del proyecto:
	-Por si no saben para que sirve el entorno virtual vean este video: https://www.youtube.com/watch?v=j5HfUYVyXBQ
	-Hay que ir a la carpeta del proyecto y antes de entrar a la mera carpeta del proyecto Django hay que instalar Virtualenv por comando.
		Dentro de la carpeta Scripts de Python310 hay que ejectutar el comando pip install virtualenv.
	-Luego hay que ejecutar en esa carpeta raíz del proyecto el comando "virtualenv env" y automaticamente se crea una carpeta llamada env.

- Instalar Django:
	-Hay que ir a la carpeta raíz del proyecto y ejecutar la terminal el siguiente comando: Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
		Este comando es para establecer politicas de ejecución para el usuario y se pueda ejecutar el venv sin ningún problema. (No es necesario en MacOS, solo se ejecuta ". env/bin/activate")
	-En la raíz del proyecto activar el venv con ell siguiente comando: env\Scripts\activate
	-Luego ejecutar los siguientes comandos de instalación : 
		* pip install django
		* pip install mysql
		* pip install django-rest-action-permissions
		* pip install python-decouple 
		* pip install djangorestframework-simplejwt
	-Listo y así mismo se instala dentro del venv cualquier otro paquete que se vaya a ocupar de python.

- Crear en la raíz del proyecto un archivo db.cnf con los siguientes parametros para conectarse a la db:
		
			*[client]
			*database = ACCO_MANAG
			*user = UsuarioDeDB
			*password = ContraseñaDelUsuarioDB
			*host = localhost
			*port = 
			*default-character-set = utf8
			
- Crear en la raíz del proyecto un archivo .env para manejar variables de entorno(Solicitar las respectivas variables a alguien del equipo).

- A partir de acá son explicaciones de los pasos que hice para crear el proyecto django y no lo tienen que hacer por que solo van a obtenerlo del repositorio:

	-Configurar el visual studio code para python: https://www.youtube.com/watch?v=eFThEXvuZaM
	-En la carpeta raíz del proyecto y dentro del venv cree el proyecto Django con el comando: django-admin startproject Acco_Manag_Project
	-Luego se creó la app de General para el proyecto: django-admin startapp General.
		Recordar que una app se refiere a un módulo de desarrollo.
	-Luego en el archivo settings.py de la carpeta con el mismo nombre del proyecto donde está el admin. Se modifica la parte de Installed Apps y se agrega esa línea luego de la coma:
		'Acco_Manag_Project.General'
	-Luego en la parte de la base de datos se modifica el "Name:" con lo siguiente: "Name": "Acco_Manag.db". Con eso se asigna una base de datos sqlite3.
	-Se le configura el idioma y zona horaria: LANGUAGE_CODE = 'es-cr' 
		TIME_ZONE = 'America/Costa_Rica'
	-Crear un super usuario para ingresar al admin: python manage.py createsuperuser
	-Correr el server: python manage.py runserver
	
