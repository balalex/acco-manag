import imp
from multiprocessing import context
from django.contrib import messages
from multiprocessing.spawn import import_main_path
from pickle import NONE
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .models import Banco, Departamento, Periodo, Compania
from Nominas.models import Cliente
from .forms import ClienteForm
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from django.contrib.auth.hashers import make_password
from General.Sender import Sender_Email
from General.views import accion_bitacora
# Create your views here.
def banco(request):
    bancosLista = Banco.objects.all()
    return render(request,"banco.html",{'Bancos':bancosLista})

def clientesListados(request):
    clienteList= Cliente.objects.all()    
    return render(request,"clientesLista.html",{"Clientes": clienteList})

def form_banco(request):
    departamentos= Departamento.objects.all()  
    return render(request,"form_banco.html",{'Departamentos':departamentos})

def registrar_banco(request, *args, **kwargs):   
    departamento = Departamento.objects.get(id=int(request.POST.get('departamento',False)))
    codigo = request.POST.get('codigo',False)    
    estado = request.POST.get('estado',False)  
    Banco.objects.create( IdDepartamento=departamento,Codigo=codigo, Estado=estado)    
    accion_bitacora(request,'Crear Banco','Se ha registrado el banco '+codigo)
    messages.success(request, '¡Banco Agregado!')
    return redirect('/banco')

def eliminar_banco(request, id):
    banco = Banco.objects.get(id=id)
    banco.delete()            
    accion_bitacora(request,'Eliminar Banco','Se ha eliminado el banco '+banco.Codigo)
    messages.success(request, '¡Banco Eliminado!')
    return redirect('/banco')

def edicion_banco(request, id):
    banco = Banco.objects.get(id=id)
    departamentosListados = Departamento.objects.all()
    return render(request,'editar_banco.html', {'Banco':banco,"Departamentos":departamentosListados,'id':id})

def editar_banco(request, id, *args, **kwargs):   
    departamento = Departamento.objects.get(id=int(request.POST.get('departamento',False)))
    codigo = request.POST.get('codigo',False)    
    estado = request.POST.get('estado',False) 
    banco=Banco.objects.get(id=id)     
    banco.IdDepartamento=departamento
    banco.Codigo=codigo    
    banco.Estado=estado
    banco.save()
    
    accion_bitacora(request,'Modificar Banco','Se ha modificado el banco '+banco.Codigo)
    messages.success(request, '¡Banco Actualizado!')
    return redirect('/banco')

class ClienteCreate(CreateView):
    model = Cliente
    template_name = 'clientes.html'
    form_class = ClienteForm 
    success_url = banco

    def get_context_data(self, **kwargs):
        context = super(ClienteCreate, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        if form.is_valid():
            cliente = form.save(commit=False)
            nombreCompleto = request.POST.get('NombreCompleto',NONE)            
            cedula = request.POST.get('Cedula',NONE)
            correo = request.POST.get('E_Mail',NONE)
            usuario = nombreCompleto[0:3]+cedula[-4:]
            password = get_random_string(length=12)            
            password_hash = make_password(password)
            cliente.IdUsuario = User.objects.create(username = usuario , password = password_hash,email=correo)
            cliente.save()            
            accion_bitacora(request,'Crear Cliente','Se ha registrado el cliente '+cedula)
            accion_bitacora(request,'Crear Usuario','Se ha registrado el usuario '+usuario)            
            print(password)
            Sender_Email(correo, nombreCompleto,usuario, password)
            clienteList= Cliente.objects.all()
            return redirect("/home")
        else:
            return self.render_to_response(self.get_context_data(form=form),{"Clientes": clienteList})
