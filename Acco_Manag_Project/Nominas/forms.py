from django import forms
from .models import Empleado, TipoBeneficio, Puesto, Rol

class EmpleadoForm(forms.ModelForm):

    class Meta:
        model = Empleado
        fields = [
            'Nombre',
            'PrimerApellido',
            'SegundoApellido',
            'TipoCedula',
            'Cedula',
            'Direccion',
            'Telefono',
            'FechaIngreso',
            'Salario',
            'IdTipoBeneficio',
            'SaldoVacaciones',
            'BonoAnual',
            'IdPuesto',
            'IdRol'
        ]
        widgets = {
             'Nombre': forms.TextInput(attrs={'class':'form-control'}),
            'PrimerApellido':forms.TextInput(attrs={'class':'form-control'}),
            'SegundoApellido':forms.TextInput(attrs={'class':'form-control'}),
            'TipoCedula':forms.Select(attrs={'class':'form-control'}),
            'Cedula':forms.TextInput(attrs={'class':'form-control'}),
            'Direccion':forms.TextInput(attrs={'class':'form-control'}),
            'Telefono':forms.TextInput(attrs={'class':'form-control'}),
            'FechaIngreso':forms.DateInput(attrs={'class':'form-control', 'type':'date'}),
            'Salario':forms.TextInput(attrs={'class':'form-control'}),            
            'SaldoVacaciones':forms.TextInput(attrs={'class':'form-control'}),
            'BonoAnual':forms.TextInput(attrs={'class':'form-control'}),
            'IdTipoBeneficio':forms.Select(attrs={'class':'form-control'}),
            'Correo':forms.TextInput(attrs={'class':'form-control'}),
            'IdPuesto':forms.Select(attrs={'class':'form-control'}),        
            'IdRol':forms.Select(attrs={'class':'form-control'}),        
        }
