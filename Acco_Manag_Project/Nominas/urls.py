from django.urls import path
from . import views

urlpatterns = [
    path('', views.nominas, name="nominas"),
    path('eliminar_nomina/<id>',views.eliminar_nomina, name="Eliminar_Nomina"),
    path('edicion_nomina/<id>',views.edicion_nomina, name="Edicion_Nomina"),
    path('edicion_nomina/editar_nomina/<id>',views.editar_nomina)  
]