$(document).ready(function () {
  $.each($(".precio"), function (index, element) {
    var precio = parseInt($(element).text().trim().substring(2));
    precio = precio.toLocaleString();
    $(element).text("₡ " + precio);
  });
});