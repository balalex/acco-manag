from django.db import models
from Periodos.models import Periodo
from Login.models import Rol
from django.contrib.auth.models import User
# Create your models here.

class Puesto(models.Model):
  puesto = models.CharField(max_length=50)
  descripcion = models.TextField()

  def __str__(self):
        return self.puesto


class TipoBeneficio(models.Model):
  TipoBeneficio = models.CharField(max_length=50)
  Descripcion = models.TextField()

  def __str__(self):
        return self.TipoBeneficio

class TipoCedula(models.Model):
  TipoCedula = models.CharField(max_length=50, unique=True)
  Descripcion = models.TextField()

  def __str__(self):
        return self.TipoCedula

class Empleado(models.Model):
  puestos = []
  for i in range(5):
    f = lambda j, i = i : i
    puestos.append(f)
  
  Nombre = models.CharField(max_length=50)
  PrimerApellido = models.CharField(max_length=50, verbose_name="Primer Apellido")
  SegundoApellido = models.CharField(max_length=50, verbose_name="Segundo Apellido")
  TipoCedula = models.ForeignKey(TipoCedula, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Tipo de Cédula")
  Cedula =models.CharField(max_length=50, verbose_name="Cédula")
  Direccion = models.TextField(verbose_name="Dirección")
  Telefono = models.CharField(max_length=20, verbose_name="Teléfono")
  FechaIngreso = models.DateField(verbose_name="Fecha de Ingreso")
  Salario = models.FloatField()
  IdTipoBeneficio = models.ForeignKey(TipoBeneficio, on_delete=models.CASCADE,verbose_name="Tipo de Beneficio")
  SaldoVacaciones = models.CharField(max_length=30, verbose_name="Saldo Vacaciones")
  BonoAnual = models.FloatField(verbose_name="Bono Anual")
  IdPuesto = models.ForeignKey(Puesto, on_delete=models.CASCADE, verbose_name="Puesto")
  IdUsuario = models.OneToOneField(User, on_delete=models.CASCADE,default=None, verbose_name="Usuario")
  IdRol = models.ForeignKey(Rol, on_delete=models.CASCADE, verbose_name="Rol")

  def __str__(self):
        return self.Nombre

class TipoCliente(models.Model):
  TipoCliente= models.CharField(max_length=30)
  
  def __str__(self):
        return self.TipoCliente

class Cliente(models.Model):
  NombreCompleto = models.CharField(max_length=100, verbose_name="Nombre Completo")
  TipoCedula= models.ForeignKey(TipoCedula, on_delete= models.CASCADE, blank=True, null=True, verbose_name="Tipo de Cédula")
  Cedula = models.CharField(max_length=50, verbose_name="Cédula")
  Direccion = models.TextField(verbose_name="Dirección")
  RazonSocial= models.TextField(verbose_name="Razón Social")
  E_Mail = models.CharField(max_length=50, verbose_name="Correo")
  IdTipoCliente= models.ForeignKey(TipoCliente,on_delete=models.CASCADE, verbose_name="Tipo de Cliente")
  IdUsuario = models.OneToOneField(User,on_delete=models.CASCADE, verbose_name="Usuario")
  IdRol=models.ForeignKey(Rol,on_delete=models.CASCADE, verbose_name="Rol")

  def __str__(self):
        return self.NombreCompleto

class Compania(models.Model):
  Nombre = models.CharField(max_length=50)
  Direccion = models.TextField()
  Telefono = models.CharField(max_length=20)
  E_mail = models.CharField(max_length=50)
  tipoCedula = models.ForeignKey(TipoCedula,on_delete=models.CASCADE,default=None,null=False, blank=False)
  Cedula= models.CharField(max_length= 50)
  IdCliente = models.ManyToManyField(Cliente)
  
  def __str__(self):
        return self.Nombre
  
class Departamento(models.Model):
  Codigo = models.CharField(max_length=20)
  Departamento = models.CharField(max_length=50)
  Capacidad = models.IntegerField()
  Costo= models.FloatField()
  def __str__(self):
        return self.Departamento

class Nomina(models.Model):
  IdPeriodo = models.ForeignKey(Periodo,on_delete=models.CASCADE)
  IdCompania = models.ForeignKey(Compania,on_delete=models.CASCADE)
  IdEmpleado = models.OneToOneField(Empleado,on_delete=models.CASCADE)
  IdDepartamento= models.ForeignKey(Departamento,on_delete=models.CASCADE)
  Monto = models.FloatField()
  def __str__(self):
        return self.IdEmpleado.Nombre


  