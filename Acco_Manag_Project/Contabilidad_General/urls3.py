from django.urls import path
from . import views

urlpatterns = [
    path('', views.edit_balance_page, name="editar_balance")
]