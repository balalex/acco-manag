from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

def Sender_Email(correo, nombre_completo,usuario, password):
    email = EmailMultiAlternatives(
                'Bienvenido al sistema Acco-Manag',
                'Hola '+nombre_completo+', le hemos otorgado acceso al sistema y para que pueda usar de forma correcta su usuario es '+usuario+' y su clave temporal es '+password,
                settings.EMAIL_HOST_USER,
                [correo]
            )
    email.send()

#def Sender_Simple_Email(subject, message, recipient_list):
#   email = EmailMultiAlternatives(
#                subject,
#                message,
#                settings.EMAIL_HOST_USER,
#                [recipient_list]
#           )
#    email.send()

def Sender_Simple_Email(subject, template_path, context, recipient_list):
    # Renderizar el template con el contexto
    message = render_to_string(template_path, context)
    email = EmailMultiAlternatives(
                subject,
                message,
                settings.EMAIL_HOST_USER,
                [recipient_list]
            )
    # Agregar el contenido del template como alternativa de HTML al correo
    email.attach_alternative(message, 'text/html')
    email.send()
