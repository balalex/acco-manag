from django.urls import path
from . import views

urlpatterns = [
    path('', views.balance_general, name="balance_general")
]