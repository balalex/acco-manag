from django.apps import AppConfig


class ActivosFijosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Activos_Fijos'
