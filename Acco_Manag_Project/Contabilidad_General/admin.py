from django.contrib import admin
from .models import  BalanceGeneral
from Cuenta_Bancaria.models import CuentaBancaria
from Periodos.models import Periodo
from Nominas.models import Compania, Departamento, Cliente
# Register your models here.


admin.site.register(BalanceGeneral)