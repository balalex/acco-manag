from django import forms
from Nominas.models import Cliente, TipoCliente

class ClienteForm(forms.ModelForm):

    class Meta:
        model = Cliente
        fields = [
            'NombreCompleto',
            'TipoCedula',
            'Cedula',
            'Direccion',            
            'RazonSocial',
            'E_Mail',
            'IdTipoCliente',
            'IdRol'
        ]
        widgets = {
             'NombreCompleto': forms.TextInput(attrs={'class':'form-control'}),            
             'TipoCedula':forms.Select(attrs={'class':'form-control'}),            
            'Cedula':forms.TextInput(attrs={'class':'form-control'}),            
            'Direccion':forms.TextInput(attrs={'class':'form-control'}),
            'RazonSocial':forms.TextInput(attrs={'class':'form-control'}),
            'E_Mail':forms.TextInput(attrs={'class':'form-control'}),              
            'IdTipoCliente':forms.Select(attrs={'class':'form-control'}),       
            'IdRol':forms.Select(attrs={'class':'form-control'})                                              
        }
