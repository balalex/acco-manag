
from django.db import models
from Periodos.models import Periodo
from Nominas.models import Compania, Departamento, TipoCedula
from Login.models import Rol
from django.contrib.auth.models import User

# Create your models here.

class Banco(models.Model):
  IdDepartamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
  Codigo = models.CharField(max_length=20)
  Estado =models.BooleanField()  

  def __str__(self):
        return self.Codigo


