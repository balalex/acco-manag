from django.urls import path
from . import views
from Nominas import views as VN
from Banco import views as VB

urlpatterns = [
    path('', views.home, name="home"),    
    path('empleados',VN.EmpleadoCreate.as_view(), name="empleados"),
    path('clientes',VB.ClienteCreate.as_view(), name="clientes")
]