from django.contrib import messages
from django.shortcuts import redirect, render
from .models import Periodo
from django.contrib.auth.models import User
from General.views import accion_bitacora
from datetime import datetime

# Create your views here.
def periodos(request):
    periodo = Periodo.objects.all()
    return render(request,"periodos.html", {"Periodos":periodo})

def editar_periodo(request):
    return render(request,"editar_periodo.html")

def registrar_periodo(request, *args, **kwargs):
    codigo = request.POST.get('codigo',False)    
    inicio_str = request.POST.get('fecha_inicio',False)
    inicio = datetime.strptime(inicio_str, '%Y-%m-%d')
    final_str = request.POST.get('fecha_final',False)
    final = datetime.strptime(final_str, '%Y-%m-%d')

    Periodo.objects.create(Codigo= codigo,FechaInicio= inicio,FechaFin= final)    
    accion_bitacora(request,'Crear Periodo','Registro del periodo '+codigo)
    messages.success(request, '¡Periodo Abierto!')

    return redirect('/periodos')

def editar_estado(request, id):
    periodo = Periodo.objects.get(id=id)
    periodo.Estado = 0
    periodo.save()      
    accion_bitacora(request,'Modificar Periodo','Se ha modificado el estado del periodo '+periodo.Codigo)
    messages.success(request, '¡Periodo Cerrado!')
    return redirect('/periodos')