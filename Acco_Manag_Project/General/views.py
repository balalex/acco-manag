import imp
from urllib import request
from django.contrib import messages
from django.shortcuts import render, redirect
from .models import Bitacora
from datetime import datetime
from django.contrib.auth.models import User
# Create your views here.

def home(request):
    bitacorasList = Bitacora.objects.all()
    return render(request,"general.html", {'Bitacoras':bitacorasList})

def accion_bitacora(request, accion, descripcion):
    usuario = User.objects.get(username=request.user)
    date = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    descriptionComplete = date+' | '+descripcion
    Bitacora.objects.create(IdUsuario = usuario, Accion = accion,Descripcion = descriptionComplete, Fecha = date)
