import random
import string

from django import forms
from django.conf import settings
from django.contrib import auth, messages
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User, auth
from django.core.mail import send_mail
from django.shortcuts import redirect, render
from django.utils.crypto import get_random_string
from General.Sender import Sender_Simple_Email
from rest_framework_simplejwt.tokens import AccessToken


def generate_token(user):
    token = AccessToken.for_user(user)
    return str(token)


def login(request):
    return render(request, "login.html")


def recuperar_contrasena(request):
    return render(request, "recuperar_contrasena.html")


def cambiar_contrasena(request):
    return render(request, "cambiar_contrasenna.html")


def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            request.session['token'] = generate_token(user)
            return redirect('home/')
        else:
            messages.error(request, 'Usuario o contraseña incorrectos')
    return render(request, 'login.html')


def logout(request):
    auth.logout(request)
    return redirect('login')


def home(request):
    return redirect('home')

def error_404(request):
    return render(request, '404.html')

def sendCode(request):
    if request.method == 'POST':
        username = request.POST['username']
        try:
            user = User.objects.get(username=username)
            userMail = user.email
        except User.DoesNotExist:
            # Redirigir al usuario a la página de error si no se encuentra el usuario
            return redirect('error_404')
        code = get_random_string(12)
        request.session['changeCode'] = code
        request.session['userMail'] = userMail
        request.session['username'] = username
        context = {'Usuario':username,'Codig':code}
        Sender_Simple_Email(
            subject="Sistema AccoManag",
            context=context,
            template_path="email.html",
            recipient_list=userMail
        )
        # Renderizar la página de cambiar_contraseña si el usuario y correo existen
        return render(request, 'cambiar_contrasenna.html')
    else:
        # Redirigir al usuario a la página de error si la solicitud no es POST
        return redirect('error_404')


def changePassword(request):
    sessionCode = request.session['changeCode']
    code = request.POST['code']
    newPass = request.POST['newPass']
    confirmPass = request.POST['confirmPass']
    username = request.session['username']
    if code == sessionCode:
        if newPass == confirmPass:
            passEncrypted = make_password(newPass)
            User.objects.filter(username=username).update(password=passEncrypted)
            return render(request, 'login.html')
    return render(request, 'cambiar_contrasenna.html')
