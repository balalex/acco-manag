from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_nomina, name="form_nomina"),
    path('crear_nomina', views.registrar_nomina, name="crear_nomina")
]