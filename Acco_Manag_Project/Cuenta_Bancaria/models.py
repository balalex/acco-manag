from django.db import models
from Nominas.models import Compania

from Banco.models import Banco 
from Nominas.models import Cliente

# Create your models here.
class CuentaBancaria(models.Model):
  Nombre_Tarjeta = models.CharField(max_length=50)
  Codigo_cvv = models.CharField(max_length=256)
  IdBanco= models.ForeignKey(Banco,on_delete=models.CASCADE)
  CuentaIBAN= models.CharField(max_length=50)
  IdCompania = models.ForeignKey(Compania,on_delete=models.CASCADE,default=None,null=False,blank=False)
  NumCuenta= models.CharField(max_length=50)
  numContrato= models.CharField(max_length=50)

  def __str__(self):
        return self.Nombre_Tarjeta
  