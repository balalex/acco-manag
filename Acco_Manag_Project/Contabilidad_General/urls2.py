from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_agregar_balance, name="form_agregar_balance"),
    path('register_balance', views.register_balance, name="register_balance")
]