from django.urls import path
from . import views

urlpatterns = [
    path('', views.cuentas_bancarias, name="cuentas_bancarias"),
    path('registrarCuentaBancaria', views.registrar_cuenta),
    path('eliminar_cuenta/<id>',views.eliminar_cuenta, name="Eliminar_Cuenta"),
    path('editar_cuenta_bancaria/<id>',views.edicion_cuenta, name="Edicion_Cuenta"),
    path('editar_cuenta_bancaria/editar_cuenta/<id>',views.editar_cuenta)  
]