from django.urls import path
from . import views

urlpatterns = [
    path('', views.banco, name="banco"),
    path('clientesList',views.clientesListados, name="clientesList"),
    path('eliminar_banco/<id>',views.eliminar_banco, name="Eliminar_Banco"),
    path('edicion_banco/<id>',views.edicion_banco, name="Edicion_Banco"),
    path('edicion_banco/editar_banco/<id>',views.editar_banco)      
]