from django.db import models

# Create your models here.

class Periodo(models.Model):
  Codigo = models.CharField(max_length=20, unique=True)
  FechaInicio = models.DateField()
  FechaFin= models.DateField()
  Estado = models.BooleanField(default=1)

  def __str__(self):
      return self.Codigo