from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_balance_general, name="form_balance_general")
]