"""Acco_Manag_Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.conf import settings
from django.views.static import serve
urlpatterns = [
    re_path(r'^static/(?P<path>.*)$', serve, {'document_root':settings.STATIC_ROOT}),
    path('admin/', admin.site.urls, name="admin"),
    path('home/', include('General.urls')),
    path('contabilidad_general/', include('Contabilidad_General.urls')),
    path('form_agregar_balance/', include('Contabilidad_General.urls2')),
    path('balance_general/', include('Balance_General.urls')),
    path('form_balance_general/', include('Balance_General.urls2')),
    path('cuentas_cobrar/', include('Cuentas_Cobrar.urls')),
    path('form_nueva_factura/', include('Cuentas_Cobrar.urls2')),
    path('editar_cuenta_cobrar/', include('Cuentas_Cobrar.urls3')),
    path('cuentas_pagar/', include('Cuentas_Pagar.urls')),
    path('form_nueva_factura_pagar/', include('Cuentas_Pagar.urls2')),
    path('activos_fijos/', include('Activos_Fijos.urls')),
    path('form_activo_fijo/', include('Activos_Fijos.urls2')),
    path('periodos/', include('Periodos.urls')),
    path('editar_periodo/', include('Periodos.urls2')),
    path('banco/', include('Banco.urls')),
    path('form_nuevo_recibo/', include('Banco.urls2')),
    path('nominas/', include('Nominas.urls')),
    path('form_nomina/', include('Nominas.urls2')),
    path('editar_nomina/', include('Nominas.urls3')),
    path('', include('Login.urls')),
    path('recuperar-contrasena/', include('Login.urls2')),
    path('cuentas_bancarias/', include('Cuenta_Bancaria.urls')),
    path('editar_cuenta_bancaria/', include('Cuenta_Bancaria.urls2')),

]

